@echo off

if not "%1" == "" goto %1

set FDNLS=..\..\FreeDOS\FD-NLS

set HWD=%_CWD%
if not exist source\nul cd ..\..
set SWD=%_CWD%
set APP=EDICT
if not exist source\%APP%\nul goto NoSources
if exist %FDNLS%\%APP%\%APP%.EN goto Build

echo Unable to locate translations. Please checkout the latest FD-NLS and update
echo the FDNLS=%FDNLS% setting to point to the FD-NLS project files.
goto Done

:NoSources
echo unable to locate sources
goto Done

:CopyNLS
if not exist %FDNLS%\%APP%\%2\%APP%.%2 goto End
xcopy /y %FDNLS%\%APP%\%2\%APP%.%2 NLS
goto End

:Build
set ASM=NASM.EXE
if exist %DOSDIR%\LINKS\NASM.COM set ASM=%DOSDIR%\LINKS\NASM.COM
if exist %DOSDIR%\LINKS\NASM.BAT set ASM=call %DOSDIR%\LINKS\NASM.BAT

if exist SOURCE\%APP%\NUL cd SOURCE\%APP%

if exist ..\..\BIN\%APP%.COM del ..\..\BIN\%APP%.COM
if exist ..\..\%APP%.ZIP del ..\..\%APP%.ZIP

if not exist ..\..\bin\nul mkdir ..\..\bin
if not exist ..\..\nls\nul mkdir ..\..\nls

%ASM% -IDOSKIT\ %APP%.ASM -fbin -O9 -o ..\..\BIN\%APP%.COM

cd ..\..

if not exist BIN\%APP%.COM goto Done

xcopy /y %FDNLS%\%APP%\%APP%.* NLS
veach /a+ /d %FDNLS%\%APP%\*.* /x %0 CopyNLS *

dir

zip -9 -k -r %APP%.ZIP APPINFO BIN DOC NLS SOURCE

echo ":-)"

dir bin edict.zip | grep -i edict | grep -iv Directory

:Done
cd %HWD%
set HWD=
set SWD=
set APP=
set ASM=
:End