DOSKIT is my latest NASM based library for creating optimized command line
utilities and programs (mostly for FreeDOS). It is released under GPL v2.

A newer version than the one included here may be available at:

https://gitlab.com/DOSx86/doskit