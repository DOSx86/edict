TITLE=Enhanced Disk Image Creation Tool, Version %_ %s
COPYRIGHT=Copyright (c) 2018, Jerome Shidel
LANGUAGE=Simplified Built-in English Language Text
HELP_00="EDICT options: " %r
HELP_01="  /d <drive>     set drive letter"
HELP_02="  /f <filename>  set file name"
HELP_03="  /p <limit>     maximum extra passes" %r
HELP_04="  /t <kind>      overide BIOS and set diskette parameters"
HELP_05="                 (0=360K, 1=1.2M, 2=720K, 3=1.44M)" %r
HELP_06="  /c <number>    force cylinders/tracks (0-1023)"
HELP_07="  /r <number>    force sides per track (0-255)"
HELP_08="  /s <number>    force sectors per side (0-63)"
HELP_09="  /b <number>    force bytes per sector (1-8192)" %r
HELP_0a="  /h             display help"
INVALID=%r "Invalid command: `" %s "'"
BAD_INT="Bad number"
BAD_VAL="Value out of range"
DRIVE=Drive %_ %c: (ID 0x %b ) %_
DRIVE_TYPE=%_ (ID 0x %b )
DRIVE_TYPE_00=unknown type
DRIVE_TYPE_01='5.25"' 360K
DRIVE_TYPE_02='5.25"' 1.2M
DRIVE_TYPE_03='3.5"' 720K
DRIVE_TYPE_04='3.5"' 1.44M
DRIVE_TYPE_05=other type
DRIVE_SPEC=%i %_ bytes, %_ %I %_ sectors, %_ %I %_ tracks, %_ %I %_ side(s)
PASS_LIMIT=Up to %_ %i %_ extra passes
BUFFERS=%i %_ byte data buffer, %_ %i %_ byte result buffer
PASS=Floppy read pass # %i. %_ %i %_ sector(s) remaining.
READ_FAST=READ TRACK: %_ %i, HEAD: %_ %i
READ_SLOW=READ TRACK: %_ %i, HEAD: %_ %i, SECTOR: %_ %i
FAILED=Imaging failed.
ABORTED=Imaging aborted.
COMPLETED=Imaging completed.
BErr=%r BIOS error 0x %b
DErr=%r DOS error 0x %b
