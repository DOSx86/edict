; Copyright (c) 2018, Jerome Shidel
; All rights reserved.
;
; Redistribution and use in source and binary forms, with or without
; modification, are permitted provided that the following conditions are met:
;
; * Redistributions of source code must retain the above copyright notice, this
;   list of conditions and the following disclaimer.
;
; * Redistributions in binary form must reproduce the above copyright notice,
;   this list of conditions and the following disclaimer in the documentation
;   and/or other materials provided with the distribution.
;
; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
; AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
; IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
; DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
; FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
; DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
; CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
; OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
; OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

; NASM 2.13.01

; Since this program does note execute any child processes, all memory above
; the binaries code can be used. Stack and heap do not need to be restricted.

; %idefine SIZEOF_STACK   0x0100  ; Required Stack bytes, don't need this much.
; %idefine SIZEOF_MEMORY  0x4000  ; Maximum bytes of memory program can use.

%ifndef SIZEOF_STACK
    %idefine RESERVE_STACK 0x007f ; Reserve this much memory for the stack
%endif

%idefine MAX_PASSES     0x000f  ; Default number of extra passes to attempt read

%idefine FAST_PASS      0x0f    ; And with pass = 0, the try a fast pass by
                                ; reading multiple tracks at once

%idefine REQUIRE_SOURCE         ; Require user to specify Source Drive
%idefine REQUIRE_TARGET         ; Require user to specify Target File

%include "FREEDOS.INC"          ; include FreeDOS specific code

; -----------------------------------------------------------------------------
; Program data area structure
; -----------------------------------------------------------------------------

struc tPDA

    .BEGIN          resb 0
    .ZERO           resb 1  ; Something has to be in the PDA. If there is,
                            ; you can comment this to save one byte of RAM.

    ; Comment out to use dumb/slow Standard I/O to reduce program size.
    ; Also, any Buffered File I/O adds about 300 bytes of code.
    ; IOBUFFER        STDIN
    ; IOBUFFER        STDOUT

    ; Not usually needed. So, commented out to conserve memory.
    ; IOBUFFER      STDERR
    ; IOBUFFER      STDAUX
    ; IOBUFFER      STDPRT

    .DRIVE          resb 1
    .DRIVE_LETTER   resb 1

    .DISK_SIDES     resw 1  ; byte
    .DISK_CYLINDERS resw 1  ; 10-bit
    .DISK_SECTORS   resw 1  ; byte
    .DISK_SIZE      resw 1  ; couple bits

    .READ_MAX       resw 1
    .READ_PASS      resw 1
    .READ_LEFT      resw 1

    .READ_SIDE      resw 1 ; byte
    .READ_CYLINDER  resw 1 ; 10-bit
    .READ_SECTOR    resw 1 ; byte
    .READ_ATTEMPT   resw 1 ; byte
    .READ_SIZE      resw 1

    .BUFFER_PTR     resw 1 ; Diskette I/O Buffer
    .BUFFER_SIZE    resw 1

    .MAP_PTR        resw 1 ; Diskette
    .MAP_SIZE       resw 1

    .FILE_NAME      resb 127
    .FILE_HANDLE    resw 1

    ; these are update via stosb & stosw
    .OVR_CYLINDER   resb 1 ; Override flags
    .FORCE_CYLINDER resw 1 ; Override settings
    .OVR_SIDE       resb 1
    .FORCE_SIDE     resw 1
    .OVR_SECTOR     resb 1
    .FORCE_SECTOR   resw 1
    .OVR_SIZE       resb 1
    .FORCE_SIZE     resw 1

    .ERROR_CODE     resb 1

    .TEMP           resw 1 ; Temporary data area for string formatter

    HEAP_DATA

    .END            resb 0

    STACK_DATA

endstruc

; -----------------------------------------------------------------------------
; Main Program Body
; -----------------------------------------------------------------------------


%include "PREPARE.INC"           ; include common code and macros for platform

    mov             ax, MAX_PASSES
    mov             [PDA(READ_MAX)], ax

    mov             ax, Translation_End
    mov             [PDA(BUFFER_PTR)], ax

LoadLanguage:
    GetLangFilename 'EDICT', PDA(FILE_NAME)
    jc              .Done
    mov             dx, PDA(FILE_NAME)
    mov             ax, 0x3d00  ; Open File -- Read Only
    int             0x21
    jc              .Done ; File not found or other error
    mov             bx, ax ; file handle
    mov             ah, 0x3f
    mov             dx, Translation
    mov             cx, 0x8000
    int             0x21
    pushf
    push            ax
    mov             ah, 0x3e
    int             0x21
    pop             ax
    popf
    jnc             .ResetHeap
    call            DOS_ERROR
    Terminate       al
.ResetHeap:

    add             ax, Translation
    mov             di, ax
    xor             bl, bl
    mov             [di], bl
    inc             ax
    mov             [PDA(BUFFER_PTR)], ax

.Done:
    RAM_Zero         PDA(FILE_NAME), 127

%ifdef SIZEOF_HEAP
    mov             ax, [PDA(BUFFER_PTR)]
    mov             [PDA(HEAPBEGIN)], ax
    mov             [PDA(HEAPPTR)], ax
    mov             [PDA(HEAPEND)], ax
%endif


ShowHeader:
    StdOutFmtStr    "TITLE", Version_Ptr
    StdOutCRLF
    StdOutFmtStr    "COPYRIGHT"
    StdOutCRLF
    StdOutFmtStr    "LANGUAGE"
    jc              NoLanguageMessage
    StdOutCRLF

NoLanguageMessage:
    StdOutCRLF

    %ifndef REQUIRE_SOURCE
        mov             al, 'A'
        mov             [PDA(DRIVE_LETTER)], al
    %endif

    ParseCommandLine

    %ifdef REQUIRE_SOURCE
        mov             al, [PDA(DRIVE_LETTER)]
        cmp             al, 0x00
        je              SwitchH
    %endif

    %ifdef REQUIRE_TARGET
        mov             al, [PDA(FILE_NAME)]
        cmp             al, 0x00
        je              SwitchH
    %endif

    call            DriveStats
    jnc             StatsOK
    call            BIOS_ERROR
    Terminate       [PDA(ERROR_CODE)]

StatsOK:

AllocateBuffers:
    ; Calculate Diskette Buffer and Map Size requirements
    xor             dx, dx
    mov             ax, [PDA(DISK_SIDES)]
    inc             ax
    mov             cx, [PDA(DISK_SECTORS)]
    inc             cx
    mul             cx
    mov             bx, cx ; save for later

    mov             cx, [PDA(DISK_CYLINDERS)]
    inc             cx
    mul             cx
    cmp             dx, 0x0000
    jne             .MapTooBig

    mov             [PDA(MAP_SIZE)], ax

    mov             ax, bx
    mov             cx, [PDA(DISK_SIZE)]
    mul             cx

    cmp             dx, 0x0000
    jne             .BufferTooBig

    mov             [PDA(BUFFER_SIZE)], ax

    ; Ensure won't wrap segment or overwrite stack
    mov             dx, [PDA(BUFFER_PTR)]
    add             dx, ax
    jc              .TooBig
    mov             [PDA(MAP_PTR)], dx
    add             dx, [PDA(MAP_SIZE)]
    jc              .TooBig
    add             dx, RESERVE_STACK
    jc              .TooBig

    jmp             .Done

.ShowBuffers:
    StdOutCRLF
    StdOutFmtStr    "BUFFERS", PDA(BUFFER_SIZE), PDA(MAP_SIZE)
    ret

.MapTooBig:
    xor             ax, ax
    mov             [PDA(MAP_SIZE)], ax
    jmp             .TooBig
.BufferTooBig:
    xor             ax, ax
    mov             [PDA(BUFFER_SIZE)], ax
.TooBig:
    call            .ShowBuffers
    mov             al, 0x08
    call            DOS_ERROR
    Terminate       0x08

.Done:

    call            .ShowBuffers
    RAM_Zero        [PDA(BUFFER_PTR)], [PDA(BUFFER_SIZE)]
    RAM_Fill        [PDA(MAP_PTR)], [PDA(MAP_SIZE)], 0x01
    StdOutCRLF

    mov             al, [PDA(FILE_NAME)]
    cmp             al, 0x00
    je              ReadDiskette

    ; Create output file
    mov             dx, PDA(FILE_NAME)
    mov             cx, 00100000b
    mov             ah, 0x3c
    int             0x21
    jnc             .FileCreated
    push            ax
    call            DOS_ERROR
    pop             ax
    Terminate       al

.FileCreated:
    mov             [PDA(FILE_HANDLE)], ax

ReadDiskette:

    StdOutFmtStr   "PASS_LIMIT", PDA(READ_MAX)
    StdOutCRLF

    call            CalculateRemaining
    StdOutCRLF

.ReadNextPass:

    StdOutFmtStr    "PASS", PDA(READ_PASS), PDA(READ_LEFT)
    StdOutCRLF
    DriveReset      [PDA(DRIVE)]

    xor             ax, ax
    mov             [PDA(READ_CYLINDER)], ax
.ReadNextCylinder:
    xor             ax, ax
    mov             [PDA(READ_SIDE)], ax
.ReadNextSide:
    xor             ax, ax
    mov             [PDA(READ_SECTOR)], ax
.ReadNextSector:

    mov             cx, [PDA(DISK_SECTORS)]
    mov             bx, [PDA(READ_PASS)]
    and             bl, FAST_PASS
    cmp             bl, 0x00
    je              .SetSectors
    xor             cx, cx
.SetSectors:
    inc             cx
    mov             [PDA(READ_SIZE)], cx

    call            GetCode
    cmp             ah, 0x00
    je              .ReadSkip

    cmp             bl, 0x00
    jne             .ReadSlow

.ReadFast:
    StdOutFmtStr    "READ_FAST", PDA(READ_CYLINDER), PDA(READ_SIDE)
    jmp             .ReadBlock

.ReadSlow:
    StdOutFmtStr    "READ_SLOW", PDA(READ_CYLINDER), PDA(READ_SIDE), PDA(READ_SECTOR)

.ReadBlock:
    StdOut          '    ',CR

    ReadDriveSector [PDA(DRIVE)], [PDA(READ_SIDE)], [PDA(READ_CYLINDER)], [PDA(READ_SECTOR)], [PDA(READ_SIZE)], [PDA(BUFFER_PTR)]

    jnc             .MaybeOK

.ErrorCheck:
    pushf
    push            ax
    call            BIOS_ERROR
    DriveReset      [PDA(DRIVE)]
    RAM_Zero        [PDA(BUFFER_PTR)], [PDA(BUFFER_SIZE)]
    pop             ax
    popf
    jmp             .ReadOK
.MaybeOK:
    cmp             ah, 0x00
    je              .ReadOK
    stc
    jmp             .ErrorCheck

.ReadOK:

    call            SaveData

    call            StoreCode

    ; Check for DOS Keyboard for CTRL+Break and such.
    mov             ah, 0x0b
    int             0x21
    cmp             al, 0x00
    je              .ReadSkip
    mov             ah, 0x08
    int             0x21
    cmp             al, 0x03
    jmp              .ReadAbort

.ReadSkip:
    mov             bx, [PDA(READ_PASS)]
    and             bl, FAST_PASS
    cmp             bl, 0x00
    je              .NextSide

    mov             cx, [PDA(DISK_SECTORS)]
    mov             ax, [PDA(READ_SECTOR)]
    inc             ax
    mov             [PDA(READ_SECTOR)], ax
    cmp             ax, cx
    jbe             .ReadNextSector

.NextSide:
    mov             cx, [PDA(DISK_SIDES)]
    mov             ax, [PDA(READ_SIDE)]
    inc             ax
    mov             [PDA(READ_SIDE)], ax
    cmp             ax, cx
    jbe             .ReadNextSide

    mov             cx, [PDA(DISK_CYLINDERS)]
    mov             ax, [PDA(READ_CYLINDER)]
    inc             ax
    mov             [PDA(READ_CYLINDER)], ax
    cmp             ax, cx
    jbe             .ReadNextCylinder

    call            CalculateRemaining
    mov             ax, [PDA(READ_LEFT)]
    cmp             ax, 0x0000
    je              .ReadDone

    mov             cx, [PDA(READ_PASS)]
    cmp             cx, [PDA(READ_MAX)]
    jae             .ReadFailed
    inc             cx
    mov             [PDA(READ_PASS)], cx

    jmp             .ReadNextPass

.ReadFailed:
    StdOutCRLF
    StdOutFmtStr    "FAILED"
    mov             al, 0x01
    jmp             .ReadFinished

.ReadAbort:
    StdOutCRLF
    StdOutFmtStr    "ABORTED"
    mov             al, 0x01
    jmp             .ReadFinished

.ReadDone:
    StdOutCRLF
    StdOutFmtStr    "COMPLETED"
    mov             al, 0x00
    jmp             .ReadFinished

.ReadFinished:

    StdOutCRLF

Die:
    push            ax
    ; Close output file
    mov             bx, [PDA(FILE_HANDLE)]
    cmp             bx, 0x0000
    je              .NoFile
    mov             ah, 0x3e
    int             0x21
    jnc             .NoFile
    push            ax
    call            DOS_ERROR
    pop             ax
    Terminate       al
.NoFile:
    pop             ax
    Terminate       al

SaveData:
    pushf
    mpush           ax, bx, cx, dx
    jnc             .NoError
    mov             cx, [PDA(DISK_SIZE)]
    mov             ax, [PDA(READ_SIZE)]
    mul             cx
    RAM_Zero        [PDA(BUFFER_PTR)], cx
    mov             cx, [PDA(READ_PASS)]
    cmp             cx, 0
    je              .NoError
    jmp             .Done
.NoError:

    ; Seek Position
    xor             dx, dx
    mov             ax, [PDA(DISK_SIZE)]
    call            GetPosition
    mul             bx
    mov             cx, dx
    mov             dx, ax
    mov             bx, [PDA(FILE_HANDLE)]
    cmp             bx, 0x0000
    je              .Done
    mov             ax, 0x4200
    int             0x21
    jc              Die

    ; Write File
    mov             cx, [PDA(DISK_SIZE)]
    mov             ax, [PDA(READ_SIZE)]
    mul             cx
    mov             cx, ax
    mov             dx, [PDA(BUFFER_PTR)]
    mov             bx, [PDA(FILE_HANDLE)]
    mov             ah, 0x40
    int             0x21
    jc              Die

.Done:
    mpop            ax, bx, cx, dx
    popf
    ret


DriveStats:
    StdOutFmtStr    "DRIVE", PDA(DRIVE_LETTER), PDA(DRIVE)

    mov             al, [PDA(OVR_CYLINDER)]
    mov             bl, [PDA(OVR_SIDE)]
    and             al, bl
    mov             bl, [PDA(OVR_SECTOR)]
    and             al, bl
    mov             bl, [PDA(OVR_SIZE)]
    and             al, bl

    cmp             al, 0x01
    je              .OverrideAll

    push            es
    DriveGetParams  [PDA(DRIVE)]

    jc              .GetParamFailed

    mov             [PDA(TEMP)], bl
    cmp             bl, 0x04
    jna             .IsFloppy
    mov             bl, 0x05
.IsFloppy:
    StdOutFmtLookup "DRIVE_TYPE_", bl
    StdOutFmtStr    "DRIVE_TYPE", PDA(TEMP)

    mov             [PDA(DISK_SIDES)], dh
    mov             al, cl
    and             al, 00111111b
    dec             al
    mov             [PDA(DISK_SECTORS)], al
    mov             al, cl
    mov             cl, 0x06
    shr             al, cl
    mov             ah, ch
    xchg            al, ah
    mov             [PDA(DISK_CYLINDERS)], ax
    mov             cl, [es:di + 3]
    mov             ax, 128
    shl             ax, cl
.SetDiskSize:
    mov             [PDA(DISK_SIZE)], ax
    mov             al,[es:di + 4]
    pop             es
.OverrideAll:
    mov             bx, [PDA(FORCE_CYLINDER)]
    mov             al, [PDA(OVR_CYLINDER)]
    cmp             al, 0x00
    je              .OverrideSides
    mov             [PDA(DISK_CYLINDERS)], bx
.OverrideSides:
    mov             bx, [PDA(FORCE_SIDE)]
    mov             al, [PDA(OVR_SIDE)]
    cmp             al, 0x00
    je              .OverrideSectors
    mov             [PDA(DISK_SIDES)], bx
.OverrideSectors:
    mov             bx, [PDA(FORCE_SECTOR)]
    mov             al, [PDA(OVR_SECTOR)]
    cmp             al, 0x00
    je              .OverrideSize
    mov             [PDA(DISK_SECTORS)], bx
.OverrideSize:
    mov             bx, [PDA(FORCE_SIZE)]
    mov             al, [PDA(OVR_SIZE)]
    cmp             al, 0x00
    je              .ShowSpecs
    mov             [PDA(DISK_SIZE)], bx

.ShowSpecs:
    StdOutCRLF
    StdOutFmtStr    "DRIVE_SPEC", PDA(DISK_SIZE), PDA(DISK_SECTORS), PDA(DISK_CYLINDERS), PDA(DISK_SIDES)
    clc
    jmp             .Done
.GetParamFailed:
    pop             es
    stc
.Done:
    ret

; Check map for successes. Only valid after first pass.
CalculateRemaining:
    mpush       ax, bx, cx, si
    mov         si, [PDA(MAP_PTR)]
    mov         cx, [PDA(MAP_SIZE)]
    mov         bx, cx
.CalcLoop:
    lodsb
    cmp         al, 0x00
    jne         .NoSub
    dec         bx
.NoSub:
    loop        .CalcLoop
    mov         [PDA(READ_LEFT)], bx
    mpop        ax, bx, cx, si
    ret

StoreCode:
    jc              .HadError
    xor             ah, ah
    jmp             .SetData
.HadError:
    cmp             ah, 0x00
    jne             .SetData
    mov             ah, 0xff
.SetData:
    mpush           di, bx, cx
    mov             di, [PDA(MAP_PTR)]
    call            GetPosition
    mov             cx, [PDA(READ_SIZE)]
.Loop:
    mov             al, [di + bx]
    cmp             al, 0x00
    je              .NoSet
    mov             [di + bx], ah
.NoSet:
    inc             di
    loop            .Loop
    mpop            di, bx, cx
    ret

GetCode:
    mpush           di, bx, cx
    mov             di, [PDA(MAP_PTR)]
    call            GetPosition
    mov             cx, [PDA(READ_SIZE)]
    xor             ah, ah
.Loop:
    mov             al, [di + bx]
    or              ah, al
    inc             di
    loop            .Loop
    mpop            di, bx, cx
    ret

GetPosition:
    mpush           ax, cx, dx
    ; SECTORS * SIDES * CYLINDER *
    ; SECTORS * SIDE
    ; SECTOR
    mov             cx, [PDA(DISK_SECTORS)]
    inc             cx
    mov             ax, [PDA(DISK_SIDES)]
    inc             ax
    mul             cx
    mov             bx, [PDA(READ_CYLINDER)]
    mul             bx

    mov             bx, ax

    mov             ax, [PDA(READ_SIDE)]
    mul             cx
    add             bx, ax

    add             bx, [PDA(READ_SECTOR)]
    mpop            ax, cx, dx
    ret

BIOS_ERROR:
    mov             al, ah
    xor             ah, ah
    mov             [PDA(ERROR_CODE)], ax
    StdOutFmtStr    "BErr", PDA(ERROR_CODE)
    StdOutFmtLookup "BErr_", [PDA(ERROR_CODE)]
    StdOutCRLF
    ret

DOS_ERROR:
    xor             ah, ah
    mov             [PDA(ERROR_CODE)], ax
    StdOutFmtStr    "DErr", PDA(ERROR_CODE)
    StdOutFmtLookup "DErr_", [PDA(ERROR_CODE)]
    ret

; On Switch Handler Entry
; AL = is Matched option. Upper//Lower Case, 0 (none) or 0xff (invalid).
; AH = Unmatched option character.
; BL = Separator Character, Usually " ,;:" or the like
; BH = Option Switch Character, '-/'
; DL = Switch type, 0 (no options), 1 (String), 2 (Boolean +/-)
; DH = 0 or When Boolean, 0=unspecified,  1=off, 2=on
; SI = Start of Option String
; DI = End of Option String
; BP = Offset of Switch Handler
; All registers (except DS,ES,SS,SP,BP) can be destroyed
;
; On exit, STC to abort processing

Options:
    Option SwitchI,         'i', 0  ; Information
    Option SwitchD,         'd', 1  ; drive
    Option SwitchF,         'f', 1  ; file name
    Option SwitchP,         'p', 1  ; pass override

    Option SwitchT,         't', 1  ; type override bios values

    Option SwitchC,         'c', 1  ; cylinders/tracks override
    Option SwitchS,         's', 1  ; sectors override
    Option SwitchR,         'r', 1  ; read heads override
    Option SwitchB,         'b', 1  ; bytes per sector override

    Option SwitchH,         'H', 0  ; help screen
    Option SwitchH,         '?', 0  ; help screen
    Option SwitchNone,      0x00
    Option SwitchInvalid,   0xff
    Option End

SwitchInvalid:
    add                 di, 0x0002
SwitchNone:
    xor                 ah, ah
    mov                 al, [di]
    mov                 [di], ah
    mov                 [PDA(TEMP)], si
    StdOutFmtStr        "INVALID", PDA(TEMP)
    mov                 [di], al
.Loopy:
    StdOutCRLF
    Terminate           0x01

SwitchI:
    call                DriveStats
    Terminate           0x00

SwitchD:
    mov                 dl, [si]
    DriveMapFromLetter    dl
    mov                 [PDA(DRIVE)], dl
    DriveMapToLetter    dl
    mov                 [PDA(DRIVE_LETTER)], dl
    ret

SwitchF:
    RAM_Zero            PDA(FILE_NAME), 127
    xor                 bx, bx
.Loop:
    lodsb
    mov                 [PDA(FILE_NAME) + bx], al
    inc                 bx
    cmp                 si, di
    jne                 .Loop
    ret

; NumberParam Ported from V8Power Tools (cause it works and I don't feel like
; rewriting it right now)

NumberParam:
    xchg        si, di
    xor         ax, ax
    push        di
 .NumLoop:
    cmp         cx, 0
    je          .Done
    mov         bl, [di]
    inc         di
    cmp         bl, 'x'
    je          .ForceHex
    cmp         bl, 'X'
    je          .ForceHex
    cmp         bl, 0x41
    jl          .NotUpperCase
    cmp         bl, 0x5a
    jg          .NotUpperCase
    jmp         .IsHex
.NotUpperCase:
    cmp         bl, 0x61
    jl          .NotLowerCase
    cmp         bl, 0x7a
    jg          .NotLowerCase
    jmp         .IsHex
.NotLowerCase:
    push        cx
    sub         bl, 0x30
    mov         cx, 10
    mul         cx
    xor         bh, bh
    add         ax, bx
    pop         cx
    loop        .NumLoop
    jmp         .Done
 .ForceHex:
    pop         bx          ; discard di
    dec         cx
    jmp         .DoAsHex
 .IsHex:
    pop         di
    mov         cx, si
    sub         cx, di
    jmp         .DoAsHex
 .Done:
    pop         di
    ret
.DoAsHex:
    xor         ax, ax
    cmp         cx, 0
    je          .SwitchError
    ; cmp         cx, 4
    ; jle         .MakeHex
    jmp         .MakeHex
.MakeHex:
    push        cx
    mov         cl, 4
    shl         ax, cl
    pop         cx
    mov         bl, [di]
    inc         di
    cmp         bl, 0x30
    jl          .SwitchError
    cmp         bl, 0x39
    jg          .NotNumber
    sub         bl, 0x30
    jmp         .Adjusted
.NotNumber:
    cmp         bl, 0x41
    jl          .SwitchError
    cmp         bl, 0x46 ; 5a is Z
    jg          .NotUpper
    sub         bl, 0x37
    jmp         .Adjusted
.NotUpper:
    cmp         bl, 0x61
    jl          .SwitchError
    cmp         bl, 0x66 ; 7a is z
    jg          .SwitchError
    sub         bl, 0x57
.Adjusted:
    xor         bh, bh
    add         ax, bx
    loop        .MakeHex
    ret
.SwitchError:
    StdOutFmtStr        "BAD_INT"
    Terminate           0x01

SwitchP:
    call                NumberParam
    mov                 [PDA(READ_MAX)], ax
    ret

OutOfRange:
    StdOutFmtStr       "BAD_VAL"
    Terminate           0x01

SwitchC:
    call                NumberParam
    cmp                 ax, 1023
    ja                  OutOfRange
    mov                 [PDA(FORCE_CYLINDER)], ax
    mov                 al, 0x01
    mov                 [PDA(OVR_CYLINDER)], al
    clc
    ret

SwitchS:
    call                NumberParam
    cmp                 ax, 63
    ja                  OutOfRange
    mov                 [PDA(FORCE_SECTOR)], ax
    mov                 al, 0x01
    mov                 [PDA(OVR_SECTOR)], al
    clc
    ret

SwitchR:
    call                NumberParam
    cmp                 ax, 255
    ja                  OutOfRange
    mov                 [PDA(FORCE_SIDE)], ax
    mov                 al, 0x01
    mov                 [PDA(OVR_SIDE)], al
    clc
    ret

SwitchB:
    call                NumberParam
    cmp                 ax, 8192
    ja                  OutOfRange
    mov                 [PDA(FORCE_SIZE)], ax
    mov                 al, 0x01
    mov                 [PDA(OVR_SIZE)], al
    clc
    ret


%idefine DriveTableSize 12
SwitchT:
    mov                 al, [si]
    cmp                 al, 0x41
    je                  .AutoDetect
    cmp                 al, 0x61
    je                  .AutoDetect

    call                NumberParam
.SetType:
    cmp                 ax, ( DriveTypes_End - DriveTypes ) / DriveTableSize - 1
    ja                  OutOfRange
    mov                 cx, DriveTableSize
    mul                 cx
    mov                 si, DriveTypes
    add                 si, ax
    mov                 di, PDA(OVR_CYLINDER)
    mov                 cx, ( DriveTypes_End - DriveTypes ) / DriveTableSize
    cld
.CopySettings:
    mov                 al, 0x01
    stosb
    lodsw
    stosw
    loop                .CopySettings
    clc
    ret
.AutoDetect:
    call                GetDOSSectors
    jmp                 .SetType

GetDOSSectors:
    push                ds
    mov                 dl, [PDA(DRIVE)]
    inc                 dl               ; Drive 0 is current
    mov                 ah, 0x1c
    int                 0x21
    mov                 ah, [ds:bx]
    pop                 ds
    ; GetTableByTotalSectors
    xor                 ah, ah
    mul                 dx
    push                ax
    StdOutFmtStr        "DOSSECT"
    jc                  .NoText
    pop                 ax
    push                ax
    StdOutInt           ax
    StdOutCRLF
.NoText:
    pop                 ax
    mov                 dx, ax
    xor                 cx, cx
    mov                 si, DriveTypes
.Looping:
    cmp                 cx, ( DriveTypes_End - DriveTypes ) / DriveTableSize
    je                  .NotFound
    mov                 bx, [si + 8]    ; total sectors
    cmp                 bx, dx
    je                  .Found
    mov                 bx, [si + 10]   ; dos reported sectors
    cmp                 bx, dx
    je                  .Found
    inc                 cx
    add                 si, DriveTableSize
    jmp                 .Looping
.Found:
    mov                 ax, cx
    ret
.NotFound:
    xor                 ax, ax
    ret

SwitchH:
    StdOutFmtStr    'HELP'
    xor             bl, bl
.Loopy:
    StdOutFmtLookup "HELP_", bl
    jc              .Done
    StdOutCRLF
    inc             bl
    jmp             .Loopy
.Done:
    StdOutCRLF
    Terminate       0

DriveTypes:         ; tracks,heads,sectors,bytes,total sectors,formatted dos
    dw              39,1,8,512,720,708        ; 360K
    dw              79,1,14,512,2240,2371     ; 1.2M
    dw              79,1,8,512,1440,1426      ; 720K
    dw              79,1,17,512,2880,2847     ; 1.44M
DriveTypes_End:

Version_Ptr:
    dw              Version_Number
Version_Number:
    db              '0.10',0

; -----------------------------------------------------------------------------
; Include required Functions, Constants, Variables, Stack, Heap ant etc.
; -----------------------------------------------------------------------------
%include "CONCLUDE.INC"

%ifdef SIZEOF_STACK
    times           PDA(END) - BSS_START + SIZEOF_STACK + STACK_MARGIN db 0
%else
    times           PDA(END) - BSS_START db 0
%endif

Translation:
    ; Include ascii string resource file
    incbin          "edict.res"
    db              0x00

Translation_End:
