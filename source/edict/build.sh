#!/bin/bash

fdnls=../../FreeDOS/FD-NLS

swd="${PWD}"
if [[ ! -d source/edict ]] ; then
    cd ../..
    swd="${PWD}"
fi
if [[ ! -d source/edict ]] ; then
    echo unable to locate sources.
    exit 1
fi

app="${PWD##*/}"

if [[ ! -e "${fdnls}/${app}/${app}.en" ]] ; then
    echo Unable to locate translations. Please checkout the latest FD-NLS and update
    echo the fdnls=${fdnls} setting to point to the FD-NLS project files.
    exit 1
fi

[[ -d ${swd}/bin ]] && rm -rf "${swd}/bin"
[[ -d ${swd}/nls ]] && rm -rf "${swd}/nls"
[[ -e ${swd}/edict.zip ]] && rm ${swd}/edict.zip

mkdir -p "${swd}/bin"

cd source/edict
nasm edict.asm -Idoskit/ -fbin -O9 -o ${swd}/bin/edict.com || exit 1
cd "${swd}"
chmod +x bin/edict.com

mkdir -p "${swd}/nls/${app}"

for i in "${fdnls}/${app}/${app}".* ; do
    if [[ -f "${i}" ]] && [[ "${i//UTF}" == "${i}" ]] && [[ "${i//utf}" == "${i}" ]] &&
    [[ "${i//${app}.}" != "${i}" ]] ; then
        cp -av "${i}" "nls/${app}/" || exit 1
    fi
done

zip -9 -k -r edict.zip appinfo bin doc nls source

ls -al bin/edict.com edict.zip