; Copyright (C) 2018 Jerome Shidel
;
;   This program is free software; you can redistribute it and/or modify
;   it under the terms of the GNU General Public License as published by
;   the Free Software Foundation; either version 2 of the License, or
;   (at your option) any later version.
;
;   This program is distributed in the hope that it will be useful,
;   but WITHOUT ANY WARRANTY; without even the implied warranty of
;   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;   GNU General Public License for more details.
;
;   You should have received a copy of the GNU General Public License along
;   with this program; if not, write to the Free Software Foundation, Inc.,
;   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

; NASM 2.14rc0 for DOS

; -----------------------------------------------------------------------------
; FORWARD
; -----------------------------------------------------------------------------

%ifidn CODE_STAGE, BLOCK_FORWARD

; DOS INT 0x21, Function 0x0b -- Check Input Status Bug
; If Standard Input has been redirected, always returns keypress waiting
; Using buffered StdIn (StdIn as a file) also works around the issue

%imacro TEST_BUG_DOSBOX_21_0B 0
;    %warning "TEST for BUG_DOSBOX_21_0B"
    %ifndef STDIN_BUFFER
        %idefine BUG_DOSBOX_21_0B
    %endif
%endmacro

%endif

; -----------------------------------------------------------------------------
; CODE - Forward macro support code
; -----------------------------------------------------------------------------

%ifidn CODE_STAGE, BLOCK_CODE

%ifdef BUG_DOSBOX_21_0B
PATCH_BUG_DOSBOX_21_0B:
        %warning    "DOSBOX Standard Input pipe bug patch."
        mpush       ds, bx, cx, dx
        xor         bx, bx
        mov         dx, DATA_BUG_DOSBOX_21_0B
        mov         cx, 0x0001
        push        cs
        pop         ds
        mov         ah, 0x3f
        int         0x21
        mpop        ds, bx, cx, dx
        cmp         ax, 0
        je          .NoData
        jc          .NoData
        mov         al, [cs:DATA_BUG_DOSBOX_21_0B]
        ret
.NoData:
        xor         ax, ax
        stc
        ret
%endif

%endif

; -----------------------------------------------------------------------------
; Begin uninitialized data section
; -----------------------------------------------------------------------------

%ifidni CODE_STAGE, BLOCK_BSS

%ifdef BUG_DOSBOX_21_0B
    DATA_BUG_DOSBOX_21_0B:
        db  0
%endif

%endif
