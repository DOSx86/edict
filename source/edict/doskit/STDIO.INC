; Copyright (C) 2018 Jerome Shidel
;
;   This program is free software; you can redistribute it and/or modify
;   it under the terms of the GNU General Public License as published by
;   the Free Software Foundation; either version 2 of the License, or
;   (at your option) any later version.
;
;   This program is distributed in the hope that it will be useful,
;   but WITHOUT ANY WARRANTY; without even the implied warranty of
;   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;   GNU General Public License for more details.
;
;   You should have received a copy of the GNU General Public License along
;   with this program; if not, write to the Free Software Foundation, Inc.,
;   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

; NASM 2.14rc0 for DOS

; -----------------------------------------------------------------------------
; FORWARD
; -----------------------------------------------------------------------------

%ifidni CODE_STAGE, BLOCK_FORWARD

; Generic Standard I/O

; Returns:
;   STC and %1 = 0x00, No Input
;   CLC and %1 = 0xff, Input Waiting
%imacro StdInStatus 0-1 al
    %idefine REQUIRE_StdInStatus
    %ifnidni %1, al
        push        ax
    %endif
    call        FUNCTION_StdInStatus
    %ifnidni %1, al
        mov         %1, al
        pop         ax
    %endif
%endmacro

; Returns:
;   STC and %1 = 0x00
;   CLC and %1 = Character
%imacro StdIn 0-1 al
    %idefine REQUIRE_StdIn
    %ifnidni %1, al
        push        ax
    %endif
    call        FUNCTION_StdIn
    %ifnidni %1, al
        mov         %1, al
        pop         ax
    %endif
%endmacro

; Output ASCIIZ String
;   DS:%1, output ASCIIZ string at memory location
;   %1 String, Store String in CS and output DS:STRING+ as ASCIIZ
; Return:
;   CLC, AX ?
;   STC, AX DOS File Error code
%imacro StdOutStr 1+
    %idefine REQUIRE_StdOutStr
    %ifstr %1
            jmp         %%StrEnd
        %%Str:
            db          %1
        %%StrEnd:
        push        si
        mov         si, %%Str
        call        FUNCTION_StdOutStr
        pop         si
    %elifnum  %1
        push        si
        mov         si, %1
        call        FUNCTION_StdOutStr
        pop         si
    %else
        %ifnidni %1, si
            push        si
            mov         si, %1
        %endif
        call    FUNCTION_StdOutStr
        %ifnidni %1, si
            pop         si
        %endif
    %endif
%endmacro

; Output 8-bit value or register as character
; Return:
;   CLC, AX ?
;   STC, AX DOS File Error code
%imacro StdOutChar 0-1 al
    %idefine REQUIRE_StdOutChar
    %ifnidni %1, dl
        push        dx
        mov         dl, %1
    %endif
    %ifdef STDOUT_BUFFER
        call        FUNCTION_StdOut
    %else
        OS_StdOut   %1
    %endif
    %ifnidni %1, dl
        pop         dx
    %endif
%endmacro

; Output some things to StdOut
;   Value < 0x100, as Character
;   Value > 0x0FF, DS:Value as ASCIIZ String Pointer
;   8-Bit Register, as Character
;   16-bit Register, as DS:REGISTER as ASCIIZ String Pointer
;   String, Store String + 0 in CS and output DS:STRING as ASCIIZ
; Return:
;   CLC, AX ?
;   STC, AX DOS File Error code
%imacro StdOut 0-* al
    %ifidni %0, 0
        ; StdOutChar  al
    %else
        %rep %0
            DefineRegisterType %1
            %ifstr %1
                ; %warning String %1
                StdOutStr %1,0
            %elifdef RegisterType16Bit
                ; %warning 16-bit Address %1
                StdOutStr %1
            %elifdef RegisterType8Bit
                ; %warning 8-bit Register %1
                StdOutChar %1
            %elifnum %1 < 0x100
                %ifnidni %1, 0
                    ; %warning Char %1
                    StdOutChar %1
                %endif
            %else
                ; %warning Location %1
                StdOutStr %1
            %endif
            %rotate 1
        %endrep
    %endif
%endmacro

%imacro StdOutCRLF 0
    %idefine    REQUIRE_StdOutCRLF
    call        FUNCTION_StdOutCRLF
%endmacro

; Flush the StdOut Buffer Now
; Return:
;   CLC, AX ?
;   STC, AX DOS File Error code
%imacro StdOutFlush 0
    %ifdef STDOUT_BUFFER
        %idefine REQUIRE_StdOutFlush
        call FUNCTION_StdOutFlush
    %endif
%endmacro

; Display Integer
;   %1 Value, as number
;   %1 Register, as number
; Return:
;   CLC, Ok
;   STC, File Error
%imacro StdOutInt 0-* al
%idefine REQUIRE_StdOutChar
    %rep %0
        %ifnidni %1, ax
            push        ax
        %endif
        DefineRegisterType %1
        %ifdef RegisterType8Bit
            %idefine REQUIRE_StdOutInt
            %ifnidni %1, al
                mov     al, %1
            %endif
            xor     ah, ah
            call    FUNCTION_StdOutInt
        %else
            %idefine REQUIRE_StdOutInt
            %ifnidni %1, ax
                mov     ax, %1
            %endif
            call    FUNCTION_StdOutInt
        %endif
        %ifnidni %1, ax
            pop         ax
        %endif
    %rotate 1
    %endrep
%endmacro

; Display Hexidecimal
;   %1 Value, as Hex
;   %1 Register, as Hex
; Return:
;   CLC, Ok
;   STC, File Error
%imacro StdOutHex 0-* al
%idefine REQUIRE_StdOutHexByte
%idefine REQUIRE_StdOutChar
    %rep %0
        %ifnidni %1, ax
            push        ax
        %endif
        DefineRegisterType %1
        %ifdef RegisterType8Bit
            %ifnidni %1, al
                mov     al, %1
            %endif
            call    FUNCTION_StdOutHexByte
        %else
            %idefine REQUIRE_StdOutHexWord
            %ifnidni %1, ax
                mov     ax, %1
            %endif
            call    FUNCTION_StdOutHexWord
        %endif
        %ifnidni %1, ax
            pop         ax
        %endif
    %rotate 1
    %endrep
%endmacro

; On entry DS:SI is string, DS:BX is data pointers for string
%imacro StdOutFmt 0
    %idefine REQUIRE_StdOutFmt
    %idefine REQUIRE_StdOutHexByte
    %idefine REQUIRE_StdOutHexWord
    %idefine REQUIRE_StdOutHexChar
    %idefine REQUIRE_StdOutInt
    %idefine REQUIRE_ParseCommandLine
    call    FUNCTION_StdOutFmt
%endmacro

%imacro StdOutFmtStr 1-*
    jmp     %%Search

%%Value:
    db  %1,'=',0

%ifnidni %0, 1
    %%Data:
        dw  %{2:-1}
%endif

%%Search:
    mpush       si, bx
    mov         si, %%Value
%ifnidni %0, 1
    mov         bx, %%Data
%else
    xor         bx, bx
%endif
    StdOutFmt
    mpop        si, bx

%endmacro


%imacro StdOutFmtLookup 2
    jmp     %%Search

%%Value:
    db  %1,'??=',0

%%Search:
    mpush       si, bx, ax
    mov         si, %%Value
    ByteToHex   %2
    xchg        al, ah
    mov         [%%Search - 4], ax
    push        ax
    pop         ax
    xor         bx, bx
    StdOutFmt
    mpop        si, bx, ax

%endmacro


%endif

; -----------------------------------------------------------------------------
; CODE - Forward macro support code
; -----------------------------------------------------------------------------

%ifidni CODE_STAGE, BLOCK_CODE

; Generic Standard I/O

%ifdef REQUIRE_StdOutCRLF
FUNCTION_StdOutCRLF:
    StdOutStr   RESOURCE_StdOutCRLF
    ret
%endif

%ifdef REQUIRE_StdInStatus
FUNCTION_StdInStatus:
    %ifdef STDIN_BUFFER
        mov         ax, [PDA(STDIN_CNT)]
        cmp         ax, 0
        jne         .InputWaiting
    %endif
    OS_StdInStatus
    cmp             al, 0x00
    je              .NoInput
.InputWaiting:
    mov             al, 0xff
    clc
    ret
.NoInput:
    xor         ax, ax
    stc
    ret
%endif

%ifdef REQUIRE_StdIn
FUNCTION_StdIn:
    %ifdef REQUIRE_StdInStatus
        call        FUNCTION_StdInStatus
        jc          .NoInput
    %else
        %ifdef STDIN_BUFFER
            mov         ax, [PDA(STDIN_CNT)]
            cmp         ax, 0
            jne         .InputWaiting
        %endif
        OS_StdInStatus
        cmp             al, 0x00
        je              .NoInput
    %endif
.InputWaiting:
    %ifdef STDIN_BUFFER
        FileBufferRead PDA(STDIN), PDA(STDIOCHAR), 1
        jc          .NoInput
        cmp         ax, 0
        je          .NoInput
        xor         ah, ah
        mov         al, [PDA(STDIOCHAR)]
    %else
        OS_StdIn
    %endif
    ret
.NoInput:
    xor         ax, ax
    stc
    ret
%endif

%ifdef STDOUT_BUFFER
    %ifdef REQUIRE_StdOutChar
    FUNCTION_StdOut:
        mov             [PDA(STDIOCHAR)], dl
        FileBufferWrite PDA(STDOUT), PDA(STDIOCHAR), 1
        ret
    %endif

    %ifdef REQUIRE_StdOutFlush
    FUNCTION_StdOutFlush:
        FileBufferFlush PDA(STDOUT)
        ret
    %endif
%endif

%ifdef REQUIRE_StdOutStr
FUNCTION_StdOutStr:
    mpush       dx, si
.Looping:
    mov         dl, [si]
    cmp         dl, 0
    je          .Done
    %ifdef STDOUT_BUFFER
        mov             [PDA(STDIOCHAR)], dl
        FileBufferWrite PDA(STDOUT), PDA(STDIOCHAR), 1
        jc              .Done
    %else
        OS_StdOut   dl
    %endif
    inc         si
    jmp         .Looping
.Done:
    mpop        dx, si
    ret
%endif

%ifdef REQUIRE_StdOutInt
FUNCTION_StdOutInt:
    push        ax
	push		bx
	push		cx
	push		dx
	mov         bx, 0x000a
	mov         cx, 0x0001
.TestLoop:
	cmp         ax, bx
	jae         .TenOrMore
	push        ax
.UnderTen:
	pop         ax
	add         al, 0x30
	StdOutChar  al
	loop        .UnderTen
	jmp         .Done
.TenOrMore:
	inc         cx
	xor         dx, dx
	div         bx
	push        dx
	jmp         .TestLoop
.Done:
	pop			dx
	pop			cx
	pop			bx
	pop         ax
	ret
%endif

%ifdef REQUIRE_StdOutHexByte
FUNCTION_StdOutHexByte:
    push        ax
    push        cx
    mov         cl, 0x04
    shr         ax, cl
    pop         cx
    call        .ShowPart
    pop         ax
    call        .ShowPart
	ret
.ShowPart:
    and         al, 0x0F
    add         al, 0x30
    cmp         al, 0x39
    jbe         .Display
    %ifdef      LowerCaseHexidecimal
        add         al, 0x27
    %else
        add         al, 0x07
    %endif
.Display:
    StdOutChar  al
    ret
%endif

%ifdef REQUIRE_StdOutHexWord
FUNCTION_StdOutHexWord:
    push        ax
    push        cx
    mov         cl, 0x08
    shr         ax, cl
    pop         cx
    call        FUNCTION_StdOutHexByte
    pop         ax
    call        FUNCTION_StdOutHexByte
	ret
%endif

; On entry DS:SI is string, DS:BX is data for string
%ifdef REQUIRE_StdOutFmt

FUNCTION_StdOutFmt:
.FindString:
    mpush       di, ax
    xor         ax, ax
    mov         [DATA_StdOutFmt], al
    mov         [DATA_StdOutFmt + 1], bx
    mov         di, Translation
.FindAgain:
    push        si
.FindLoop:
    mov         ah, [di]
    inc         di
    cmp         ah, CR
    je          .FindLoop
    cmp         ah, LF
    je          .FindLoop
    lodsb
    cmp         al, ah
    jne         .Diff
    cmp         al, '='
    jne         .FindLoop
    pop         si
    jmp         .Found
.Diff:
    pop         si
.ScanLoop:
    cmp         ah, 0x00
    je          .NoPrint
    cmp         ah, CR
    je          .FindAgain
    cmp         ah, LF
    je          .FindAgain
    mov         ah, [di]
    inc         di
    jmp         .ScanLoop

.NoPrint:
    stc
    jmp         .ByeBye
.Found:
    ParseCommandOptions di, .ParseOptions, ' ', '%'
    clc
.ByeBye:
    mpop        di, ax
    ret

.ParseOptions:
    Option .ParseR,          'r', 0  ; return
    Option .ParseC,          'c', 0  ; character
    Option .ParseB,          'b', 0  ; hex byte
    Option .ParseW,          'w', 0  ; hex word
    Option .ParseI,          'i', 0  ; integer
    Option .ParseAddOne,     'I', 0  ; integer + 1
    Option .ParseS,          's', 0  ; string
    Option .Parse_,          '_', 0  ; space
    Option .ParsePercent,    '%', 0  ; percent
    Option .ParseNone,       0x00
    Option .ParseNone,       0xff
    Option End

.ParseNone:
    mov         al, [DATA_StdOutFmt]
    cmp         al, 0x00
    je          .NoSpace
    StdOut      0x20
.NoSpace:
    inc         al
    mov         [DATA_StdOutFmt], al

.ShowLoop:
    lodsb
    cmp         al, CR
    je          .ShowLoop
    cmp         al, LF
    je          .ShowLoop
    cmp         si, di
    ja          .ShowDone
    StdOutChar  al
    jmp         .ShowLoop
.ShowDone:
    ret

.Parse_:
    StdOutChar  0x20
    ret

.ParsePercent:
    StdOutChar  0x25
    ret

.ParsePtr:
    mov         bx, [DATA_StdOutFmt + 1]
    mov         si, [bx]
    inc         bx
    inc         bx
    mov         [DATA_StdOutFmt + 1], bx
    ret

.ParseC:
    call        .ParsePtr
    lodsb
    StdOutChar  al
    jmp         .NullSpace

.ParseB:
    call        .ParsePtr
    lodsb
    StdOutHex   al
    jmp         .NullSpace

.ParseW:
    call        .ParsePtr
    lodsw
    StdOutHex   ax
    jmp         .NullSpace

.ParseI:
    call        .ParsePtr
    lodsw
    StdOutInt   ax
    jmp         .NullSpace

.ParseAddOne:
    call        .ParsePtr
    lodsw
    inc         ax
    StdOutInt   ax
    jmp         .NullSpace

.ParseS:
    call        .ParsePtr
    StdOut      [si]
    jmp         .NullSpace

.ParseR:
    StdOutCRLF
.NullSpace:
    xor         ax, ax
    mov         [DATA_StdOutFmt], al
    ret

%endif


%endif

; -----------------------------------------------------------------------------
; String and general resource area for built in macros
; -----------------------------------------------------------------------------

%ifidni CODE_STAGE, BLOCK_RESOURCES

%ifdef REQUIRE_StdOutCRLF
RESOURCE_StdOutCRLF:
    db CRLF,0
%endif

%endif


; -----------------------------------------------------------------------------
; Miscellaneous data area for built in macros
; -----------------------------------------------------------------------------

%ifidni CODE_STAGE, BLOCK_DATA

%endif

; -----------------------------------------------------------------------------
; Begin uninitialized data section
; -----------------------------------------------------------------------------

%ifidni CODE_STAGE, BLOCK_BSS

%ifdef REQUIRE_StdOutFmt
DATA_StdOutFmt:
    db 0
    dw 0
%endif

%endif
