; Copyright (C) 2018 Jerome Shidel
;
;   This program is free software; you can redistribute it and/or modify
;   it under the terms of the GNU General Public License as published by
;   the Free Software Foundation; either version 2 of the License, or
;   (at your option) any later version.
;
;   This program is distributed in the hope that it will be useful,
;   but WITHOUT ANY WARRANTY; without even the implied warranty of
;   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;   GNU General Public License for more details.
;
;   You should have received a copy of the GNU General Public License along
;   with this program; if not, write to the Free Software Foundation, Inc.,
;   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

; NASM 2.14rc0 for DOS

; SECTION _TEXT

; -----------------------------------------------------------------------------
; Support code for forward macros
; -----------------------------------------------------------------------------

%idefine CODE_STAGE BLOCK_CODE

%include 'COMMON.INC'

; -----------------------------------------------------------------------------
; String and general resource area for built in macros
; -----------------------------------------------------------------------------

%idefine CODE_STAGE BLOCK_RESOURCES

%include 'COMMON.INC'

; -----------------------------------------------------------------------------
; Miscellaneous data area for built in macros
; -----------------------------------------------------------------------------

; SECTION _DATA

%idefine CODE_STAGE BLOCK_DATA

%include 'COMMON.INC'

; -----------------------------------------------------------------------------
; Begin uninitialized data section
; -----------------------------------------------------------------------------

; SECTION _BSS

BSS_START:

%idefine CODE_STAGE BLOCK_BSS

%include 'COMMON.INC'

; -----------------------------------------------------------------------------
; Program Data Area. Required Constants, Variables, Stack and Heap starts here
; -----------------------------------------------------------------------------

PDA_START:
